# Vortigo - Teste prático - Wordpress

### Requisitos:
- PHP
- HTML
- Javascript
- Banco de dados
- Wordpress

---

## Descrição do teste

Desenvolver um blog em wordpress com o tema [Twenty Seventeen](https://br.wordpress.org/themes/twentyseventeen/) e criar um plugin para agendamento de eventos.

### Tema

 - No Tema, adicione na Home, um slide carrousel com 5 banners.
 - Na Home altere os posts de paralax para grid 2 colunas.
 - Adicione uma sidebar na direita com os 5 posts mais visualizados(ordem decresente).
 - Na sidebar adicione um calendário com os eventos criados pelo plugin.

---

### Plugin

 - Deve conter um título.
 - Campo para cadastro de dia/horário
 - Local
 - Campo para observações

---

### Forma de entrega:

Crie um fork deste repositório e após finalizado nos envie por email para luiz.ferreira@vortigo.com.br, o tema e o plugin desenvolvidos.

